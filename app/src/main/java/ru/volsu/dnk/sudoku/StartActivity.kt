package ru.volsu.dnk.sudoku

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import ru.volsu.dnk.sudoku.databinding.ActivityStartBinding
import ru.volsu.dnk.sudoku.ui.theme.GameActivity

class StartActivity : AppCompatActivity() {
    var game: Game = Game()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.QuitButton.setOnClickListener { finish(); System.exit(0) }
        binding.NewGameButton.setOnClickListener {
            game.generate(27)
            startGame()
        }
        binding.SaveGameButton.setOnClickListener {
            openFileOutput(binding.SaveNameText.text.toString() + ".json", Context.MODE_PRIVATE).use {
                it.write(Json.encodeToString(game).toByteArray())
            }
        }
        binding.ContinueButton.setOnClickListener {
            game = Json.decodeFromStream(openFileInput(binding.SaveNameText.text.toString() + ".json"))
            startGame()
        }
    }

    fun startGame () {
        val gameIntent = Intent(applicationContext, GameActivity::class.java)
        gameIntent.putExtra("gameJson", Json.encodeToString(game))
        startActivity(gameIntent)
    }
}