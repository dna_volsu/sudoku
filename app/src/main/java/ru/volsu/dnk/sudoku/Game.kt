package ru.volsu.dnk.sudoku

import kotlinx.serialization.Serializable
import kotlin.random.Random

@Serializable
class Cell {
    var hint: Int = -1
    var perm: Int = -1
    var cands: MutableList<Int> = mutableListOf()
}

@Serializable
class State {
    var level: Int = 0
    var field: Array<Array<Cell>> = arrayOf(
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(),Cell(), Cell(), Cell(),),
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell()),
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell()),
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(),Cell(), Cell(), Cell(),),
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell()),
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell()),
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(),Cell(), Cell(), Cell(),),
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell()),
        arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell()),
    )
}

@Serializable
class Game {
    var state: State = State()

    fun generate(level: Int) {
        state.level = level
        for (j in 0..26) {
            state.field[j % 9 / 3 + 3 * (j / 9)][j % 9 % 3 + 3 * (j / 9)].hint = j //Random.nextInt(1,10)
        }
    }

    fun getCell(y: Int, x: Int): Cell {
        return state.field[y][x]
    }

    fun getValue(y: Int, x: Int): Int {
        val c = state.field[y][x]
        return if (c.perm != -1) c.perm else c.hint
    }

    fun setValue(y: Int, x: Int, v: Int) {
       val c = state.field[y][x]
       if (c.hint == -1) {
           c.perm = v
       }
    }
    fun getField(): String {
        var fieldStr = ""
        for (y in 0..8) {
            var rowStr = ""
            for (x in 0..8) {
                val v = getValue(y, x)
                rowStr += (if (v != -1) v.toString() else " ") + " "
            }
            fieldStr += rowStr + "\n"
        }
        return fieldStr
    }
}