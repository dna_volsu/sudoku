package ru.volsu.dnk.sudoku.ui.theme

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import kotlinx.serialization.json.Json
import ru.volsu.dnk.sudoku.Cell
import ru.volsu.dnk.sudoku.Game
import ru.volsu.dnk.sudoku.ui.theme.ui.theme.SudokuTheme

class GameActivity : ComponentActivity() {
    var game: Game = Game()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        game = Json.decodeFromString(intent.getStringExtra("gameJson")!!)

        setContent {
            SudokuTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column {
                        for (y in 0..8) {
                            Row {
                                for (x in 0 .. 8) {
                                    Column {
                                        CellView(game.getCell(y, x))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun CellView(c: Cell) {
    val size = 40.dp
    val border = 1.dp
    var candtext = ""
    for (cand in c.cands) {
        candtext += "$cand "
    }
    Box(
        Modifier
            .size(size)
            .border(border, Color.Black)) {
        if (c.perm != -1) {// мы дали ответ в клетке
            TextButton(onClick = { /*TODO*/ }) {
                Text(text = c.perm.toString())
            }
        } else if (c.hint != -1) {// цифра стоялв изначально
            TextButton(onClick = { /*TODO*/ }) {
                Text(text = c.hint.toString())
            }
        } else {// изначально пустая, теперь только кандидаты
            TextButton(onClick = { /*TODO*/ }) {
                Text(text = candtext)
            }
        }
    }
}